# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Estonian translation of Debian-installer
#
# This translation is released under the same licence as the debian-installer.
#
# Siim Põder <siim@p6drad-teel.net>, 2007.
# Kristjan Räts <kristjanrats@gmail.com>, 2017, 2018, 2020, 2021, 2022.
#
# Thanks to following Ubuntu Translators for review and fixes:
#     Laur Mõtus
#     Heiki Nooremäe
#     tabbernuk
#
#
# Translations from iso-codes:
#   Tobias Quathamer <toddy@debian.org>, 2007.
#     Translations taken from ICU SVN on 2007-09-09
#   Alastair McKinstry <mckinstry@computer.org>, 2001,2002.
#   Free Software Foundation, Inc., 2000, 2004, 2006
#   Hasso Tepper <hasso@estpak.ee>, 2006.
#   Margus Väli <mvali@hot.ee>, 2000.
#   Siim Põder <windo@p6drad-teel.net>, 2006.
#   Tõivo Leedjärv <leedjarv@interest.ee>, 2000, 2001, 2008.
#   Mattias Põldaru <mahfiaz@gmail.com>, 2009-2012, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: grub-installer@packages.debian.org\n"
"POT-Creation-Date: 2023-09-14 20:02+0000\n"
"PO-Revision-Date: 2024-01-29 20:01+0000\n"
"Last-Translator: Kristjan Räts <kristjanrats@gmail.com>\n"
"Language-Team: Estonian <>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: boolean
#. Description
#. :sl1:
#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:1001 ../grub-installer.templates:2001
msgid "Install the GRUB boot loader to your primary drive?"
msgstr "Kas paigaldada GRUB-alglaadur su esmasele kõvakettale?"

#. Type: boolean
#. Description
#. :sl1:
#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#: ../grub-installer.templates:1001 ../grub-installer.templates:34001
msgid ""
"The following other operating systems have been detected on this computer: "
"${OS_LIST}"
msgstr "Arvutist avastati ka teisi operatsioonisüsteeme: ${OS_LIST}"

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:1001
msgid ""
"If all of your operating systems are listed above, then it should be safe to "
"install the boot loader to your primary drive (UEFI partition/boot record). "
"When your computer boots, you will be able to choose to load one of these "
"operating systems or the newly installed Debian system."
msgstr ""
"Kui ülemises nimekirjas on kõik sinu arvutis olevad operatsioonisüsteemid, "
"peaks alglaaduri paigaldamine esmasele kõvakettale (UEFI partitsioon või "
"alglaade kirje) olema ohutu. Kui su arvuti taaskäivitub, esitatakse sulle "
"valik kõigist nimekirjas olevatest operatsioonisüsteemidest - nende hulgas "
"on ka su uus Debiani süsteem."

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:2001
msgid ""
"It seems that this new installation is the only operating system on this "
"computer. If so, it should be safe to install the GRUB boot loader to your "
"primary drive (UEFI partition/boot record)."
msgstr ""
"Paistab, et käesolev paigaldus on selle arvuti ainus operatsioonisüsteem. "
"Kui see on nii, peaks alglaaduri GRUB paigaldamise esmasele kõvakettale "
"(UEFI partitsioonile/alglaade kirje) ohutu olema."

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:2001
msgid ""
"Warning: If your computer has another operating system that the installer "
"failed to detect, this will make that operating system temporarily "
"unbootable, though GRUB can be manually configured later to boot it."
msgstr ""
"Hoiatus: Kui paigaldaja ei suutnud mõnd arvutis paiknevat "
"operatsioonisüsteemi tuvastada, siis MBR'i muutmisel muutuvad tuvastamata "
"operatsioonisüsteemid ajutiselt alglaaditamatuteks, ent hiljem saab GRUBi "
"seadistada ka nende laadimiseks."

#. Type: boolean
#. Description
#. :sl3:
#: ../grub-installer.templates:3001
msgid "Install the GRUB boot loader to the multipath device?"
msgstr "Kas paigaldada GRUB-alglaadur multipath seadmele?"

#. Type: boolean
#. Description
#. :sl3:
#: ../grub-installer.templates:3001
msgid "Installation of GRUB on multipath is experimental."
msgstr "GRUB-i paigaldamine multipath seadmele on katsetamisel."

#. Type: boolean
#. Description
#. :sl3:
#: ../grub-installer.templates:3001
msgid ""
"GRUB is always installed to the master boot record (MBR) of the multipath "
"device. It is also assumed that the WWID of this device is selected as boot "
"device in the system's FibreChannel adapter BIOS."
msgstr ""
"Multipath seadmel paigaldatakse GRUB alati esmasesse alglaadimiskirjesse "
"(MBR). Vajalik on ka see, et see ketas oleks süsteemi FibreChannel adapteri "
"BIOS-i sätetes määratud esimesena alglaaditavaks."

#. Type: boolean
#. Description
#. :sl3:
#: ../grub-installer.templates:3001
msgid "The GRUB root device is: ${GRUBROOT}."
msgstr "GRUB-i juurseade on: ${GRUBROOT}."

#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:4001 ../grub-installer.templates:13001
msgid "Unable to configure GRUB"
msgstr "GRUB'i ei õnnestu seadistada"

#. Type: error
#. Description
#. :sl3:
#: ../grub-installer.templates:4001
msgid "An error occurred while setting up GRUB for the multipath device."
msgstr "Multipath seadmele GRUB-i ülesseadmisel esines viga."

#. Type: error
#. Description
#. :sl3:
#: ../grub-installer.templates:4001
msgid "The GRUB installation has been aborted."
msgstr "GRUB-i paigaldamine on katkestatud."

#. Type: string
#. Description
#. :sl2:
#. Type: select
#. Description
#. :sl2:
#: ../grub-installer.templates:5001 ../grub-installer.templates:6002
msgid "Device for boot loader installation:"
msgstr "Seade, kuhu alglaadur paigaldada:"

#. Type: string
#. Description
#. :sl2:
#. Type: select
#. Description
#. :sl2:
#: ../grub-installer.templates:5001 ../grub-installer.templates:6002
msgid ""
"You need to make the newly installed system bootable, by installing the GRUB "
"boot loader on a bootable device. The usual way to do this is to install "
"GRUB to your primary drive (UEFI partition/boot record). You may instead "
"install GRUB to a different drive (or partition), or to removable media."
msgstr ""
"Sa pead oma äsjapaigaldatud süsteemi käivitatavaks muutma. Selleks on vaja "
"käivitatavale seadmele paigaldada alglaadur GRUB. Tavaliselt paigaldatakse "
"GRUB esimasele kõvakettale (UEFI partitsioonile/alglaade kirjesse), kuid "
"soovi korral võid selle paigaldada ka mõnele teisele kettale (või "
"partitsioonile) või ka eemaldatavale meediale (USB pulk, mälukaart)."

#. Type: string
#. Description
#. :sl2:
#: ../grub-installer.templates:5001
msgid ""
"The device notation should be specified as a device in /dev. Below are some "
"examples:\n"
" - \"/dev/sda\" will install GRUB to your primary drive (UEFI partition/"
"boot\n"
"   record);\n"
" - \"/dev/sdb\" will install GRUB to a secondary drive (which may for "
"instance\n"
"   be a thumbdrive);\n"
" - \"/dev/fd0\" will install GRUB to a floppy."
msgstr ""
"Seade peaks olema määratud seadmena /dev kataloog. All on mõned näidised:\n"
" - \"/dev/sda\" GRUB paigaldatakse esimesele kettale (UEFI partitsioon/\n"
"   alglaade kirje),\n"
" - \"/dev/sdb\" GRUB paigaldatakse teisele kettale (mis võib olla näiteks\n"
"   USB mälupulk või mälukaart),\n"
" - \"/dev/fd0\" GRUB paigaldatakse flopikettale."

#. Type: select
#. Choices
#: ../grub-installer.templates:6001
msgid "Enter device manually"
msgstr "Sisesta seade käsitsi"

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:7001
msgid "GRUB password:"
msgstr "GRUB'i parool:"

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:7001
msgid ""
"The GRUB boot loader offers many powerful interactive features, which could "
"be used to compromise your system if unauthorized users have access to the "
"machine when it is starting up. To defend against this, you may choose a "
"password which will be required before editing menu entries or entering the "
"GRUB command-line interface. By default, any user will still be able to "
"start any menu entry without entering the password."
msgstr ""
"Alglaadur GRUB pakub mitmeid võimsaid interaktiivseid võimalusi, mille abil "
"võib autoriseerimata kasutaja süsteemi üle kontrolli saavutada. Selleks on "
"vaja ligipääsu alglaadivale masinale. Et selle ohu vastu kaitsta, saab "
"määrata parool, mis tuleb enne menüü valikute muutmist või käsurea "
"kasutamist sisestada. Vaikimisi saab ilma parooli sisestamata käivitada "
"ainult olemasolevaid menüüvalikuid."

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:7001
msgid "If you do not wish to set a GRUB password, leave this field blank."
msgstr "Kui sa ei soovi GRUB'le parooli määrata, siis ära sisesta midagi."

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:8001
msgid "Re-enter password to verify:"
msgstr "Sisesta parool uuesti:"

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:8001
msgid ""
"Please enter the same GRUB password again to verify that you have typed it "
"correctly."
msgstr ""
"Palun sisesta sama GRUB'i parool uuesti, et välistada vead trükkimisel."

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:9001
msgid "Password input error"
msgstr "Tõrge parooli sisestamisel"

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:9001
msgid "The two passwords you entered were not the same. Please try again."
msgstr "Sinu sisestatud paroolid olid erinevad. Palun proovi uuesti."

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:11001
msgid "GRUB installation failed"
msgstr "GRUB'i paigaldamine nurjus"

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:11001
msgid ""
"The '${GRUB}' package failed to install into /target/. Without the GRUB boot "
"loader, the installed system will not boot."
msgstr ""
"'${GRUB}' paketi paigaldamine sihtkohta into /target/ nurjus. Ilma GRUB "
"alglaadurita pole võimalik paigaldatud süsteemi käivitada."

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:12001
msgid "Unable to install GRUB in ${BOOTDEV}"
msgstr "GRUB'i paigaldamine seadmele ${BOOTDEV} nurjus"

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:12001
msgid "Executing 'grub-install ${BOOTDEV}' failed."
msgstr "'grub-install ${BOOTDEV}' käivitamine nurjus."

#. Type: error
#. Description
#. :sl2:
#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:12001 ../grub-installer.templates:13001
msgid "This is a fatal error."
msgstr "See on saatuslik viga."

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:13001
msgid "Executing 'update-grub' failed."
msgstr "'update-grub' käivitamine nurjus."

#. Type: boolean
#. Description
#. :sl4:
#: ../grub-installer.templates:15001
msgid "Install GRUB?"
msgstr "Kas paigaldada GRUB?"

#. Type: boolean
#. Description
#. :sl4:
#: ../grub-installer.templates:15001
msgid ""
"GRUB 2 is the next generation of GNU GRUB, the boot loader that is commonly "
"used on i386/amd64 PCs. It is now also available for ${ARCH}."
msgstr ""
"GRUB 2 on GNU GRUB-i edasiarendus; alglaadur, mida kasutatakse tavaliselt "
"i386/amd64 PC arvutitel. Nüüd on see saadaval ka platvormidele: ${ARCH}."

#. Type: boolean
#. Description
#. :sl4:
#: ../grub-installer.templates:15001
msgid ""
"It has interesting new features but is still experimental software for this "
"architecture. If you choose to install it, you should be prepared for "
"breakage, and have an idea on how to recover your system if it becomes "
"unbootable. You're advised not to try this in production environments."
msgstr ""
"Kuigi sellel on huvitavad uued võimalused, on sellel arhitektuuril tegemist "
"siiski veel katsetamisjärgus oleva tarkvaraga. Kui valid selle paigaldamise, "
"peaksid valmis olema tõrgeteks ning teadma, kuidas mittealglaaduvat süsteemi "
"taastada. Seda pole soovitatav töömasinates kasutada."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:16001
msgid "Installing GRUB boot loader"
msgstr "GRUB alglaaduri paigaldamine"

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:17001
msgid "Looking for other operating systems..."
msgstr "Teiste võimalike operatsioonisüsteemide otsimine..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:18001
msgid "Installing the '${GRUB}' package..."
msgstr "Paketi '${GRUB}' paigaldamine..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:19001
msgid "Determining GRUB boot device..."
msgstr "GRUB'i alglaadimisseadme määramine..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:20001
msgid "Running \"grub-install ${BOOTDEV}\"..."
msgstr "\"grub-install ${BOOTDEV}\" käivitamine..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:21001
msgid "Running \"update-grub\"..."
msgstr "\"update-grub\" käivitamine..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:22001
msgid "Updating /etc/kernel-img.conf..."
msgstr "/etc/kernel-img.conf uuendamine..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:23001
msgid "Checking whether to force usage of the removable media path"
msgstr "Irdandmekandja otsinguraja kohustatud kasutamise kontrollimine"

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:24001
msgid "Mounting filesystems"
msgstr "Failisüsteemide haakimine"

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:25001
msgid "Configuring grub-efi for future usage of the removable media path"
msgstr ""
"grub-efi seadistamine irdandmekandjate otsinguraja kasutamiseks tulevikus"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl1:
#: ../grub-installer.templates:26001
msgid "Install the GRUB boot loader"
msgstr "Paigalda GRUB alglaadur"

#. Type: text
#. Description
#. Rescue menu item
#. :sl2:
#: ../grub-installer.templates:27001
msgid "Reinstall GRUB boot loader"
msgstr "GRUB alglaaduri uuesti seadistamine"

#. Type: error
#. Description
#. :sl4:
#: ../grub-installer.templates:30001
msgid "Failed to mount ${PATH}"
msgstr "Haakimine nurjus: ${PATH}"

#. Type: error
#. Description
#. :sl4:
#: ../grub-installer.templates:30001
msgid "Mounting the ${FSTYPE} file system on ${PATH} failed."
msgstr "Failisüsteemi ${FSTYPE} haakimine teele ${PATH} nurjus."

#. Type: error
#. Description
#. :sl4:
#: ../grub-installer.templates:30001
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr "Detailid leiad failist /var/log/syslog või 4. virtuaalkonsoolilt."

#. Type: error
#. Description
#. :sl4:
#: ../grub-installer.templates:30001
msgid "Warning: Your system may be unbootable!"
msgstr "Hoiatus: Su süsteem võib olla alglaadimatu!"

#. Type: text
#. Description
#. Rescue menu item
#. :sl2:
#: ../grub-installer.templates:31001
msgid "Force GRUB installation to the EFI removable media path"
msgstr "Paigalda GRUB EFI irdandmekandja otsingurajale jõuga"

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:32001
msgid "Force GRUB installation to the EFI removable media path?"
msgstr "Kas paigaldada GRUB EFI irdandmekandja otsingurajale jõuga?"

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:32001
msgid ""
"It seems that this computer is configured to boot via EFI, but maybe that "
"configuration will not work for booting from the hard drive. Some EFI "
"firmware implementations do not meet the EFI specification (i.e. they are "
"buggy!) and do not support proper configuration of boot options from system "
"hard drives."
msgstr ""
"Tundub, et see arvuti on seadistatud alglaadima EFI kaudu, kuid võib "
"juhtuda, et seadistus ei tööta kõvakettalt alglaadimiseks, sest mõned EFI "
"püsivara teostused ei vasta EFI spetsifikatsioonile (st. need on vigased!) "
"ja nad ei toeta korralikult süsteemi kõvaketastelt alglaade sätteid."

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:32001
msgid ""
"A workaround for this problem is to install an extra copy of the EFI version "
"of the GRUB boot loader to a fallback location, the \"removable media path"
"\". Almost all EFI systems, no matter how buggy, will boot GRUB that way."
msgstr ""
"Selle probleemi lahenduseks on paigaldada täiendav koopia GRUB laadija EFI "
"versioon reservasukohta - irdandmekandja otsingurajale. Peaaegu kõik EFI "
"tostused, sõltumata sellest kui vigased nad on, suudavad sedasi GRUBi "
"käivitada."

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:32001
msgid ""
"Warning: If the installer failed to detect another operating system that is "
"present on your computer that also depends on this fallback, installing GRUB "
"there will make that operating system temporarily unbootable. GRUB can be "
"manually configured later to boot it if necessary."
msgstr ""
"Hoiatus: Kui paigaldaja ei suutnud mõnd arvutis paiknevat "
"operatsioonisüsteemi tuvastada, mis sõltub samuti sellest varuvariandist, "
"siis GRUB'i paigaldamine muutub see operatsioonisüsteem ajutiselt "
"alglaaditamatuks. Hiljem saab vajadusel seadistada GRUB'i ka nende "
"laadimiseks."

#. Type: boolean
#. Description
#. Same as grub2/update_nvram
#. :sl1:
#: ../grub-installer.templates:33001
msgid "Update NVRAM variables to automatically boot into Debian?"
msgstr "Kas seadistada NVRAMi muutujaid, et alglaadida Debian automaatselt?"

#. Type: boolean
#. Description
#. Same as grub2/update_nvram
#. :sl1:
#: ../grub-installer.templates:33001
msgid ""
"GRUB can configure your platform's NVRAM variables so that it boots into "
"Debian automatically when powered on. However, you may prefer to disable "
"this behavior and avoid changes to your boot configuration. For example, if "
"your NVRAM variables have been set up such that your system contacts a PXE "
"server on every boot, this would preserve that behavior."
msgstr ""
"GRUB oskab seadistada su platvormi NVRAMi muutujaid nii, et toite "
"sisselülitamisel käivitatakse automaatselt Debian. Kuid sa võid sellest "
"loobuda ja vältida alglaade seadistuste muutmist. Näiteks ei muuda see "
"alglaadimist siis, kui NVRAMi muutujad on seadistatud nii, et alglaadimisel "
"kasutatakse alati PXE servarit."

#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#: ../grub-installer.templates:34001 ../grub-installer.templates:35001
msgid "Run os-prober automatically to detect and boot other OSes?"
msgstr ""
"Kas käivitada os-prober automaatselt, et tuvastada ja seadistada teiste OSde "
"laadimine?"

#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#: ../grub-installer.templates:34001 ../grub-installer.templates:35001
msgid ""
"GRUB can use the os-prober tool to attempt to detect other operating systems "
"on your computer and add them to its list of boot options automatically."
msgstr ""
"GRUB saab kasutada programmi os-prober, et tuvastada su arvutis olevaid "
"teisi operatsioonisüsteeme ja lisada need automaatselt alglaaditavate "
"loendisse."

#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#: ../grub-installer.templates:34001 ../grub-installer.templates:35001
msgid ""
"If your computer has multiple operating systems installed, then this is "
"probably what you want. However, if your computer is a host for guest OSes "
"installed via LVM or raw disk devices, running os-prober can cause damage to "
"those guest OSes as it mounts filesystems to look for things."
msgstr ""
"Kui su arvutisse on paigaldatud mitu operatsioonisüsteemi, siis üldjuhul sa "
"soovid seda. Samas, kui su arvuti majutab LVM või otsepöördus salvesti(te)l "
"teisi operatsioonisüsteeme, siis võib os-prober need katki teha, sest ta "
"mauntib failisüsteeme operatsioonisüsteemi failide otsimiseks."

#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#: ../grub-installer.templates:35001
msgid ""
"os-prober did not detect any other operating systems on your computer at "
"this time, but you may still wish to enable it in case you install more in "
"the future."
msgstr ""
"os-prober ei leidnud sel korral su arvutist teisi operatsioonisüsteeme, kuid "
"sa võid selle sätte lubada juhuks, kui sa paigaldad neid tulevikus."
